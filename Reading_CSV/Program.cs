﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Reading_CSV
{
    class Program
    {
        static string sourceFileName = "";
        //static string targetFileName = "";
        static float totalLines = 0;
        static void Main(string[] args)
        {
            Thread.CurrentThread.Name = "Main";

            string inputFileName = "input2.csv";

            //read user input
            sourceFileName = inputFileName.ToString();
            totalLines = File.ReadLines(inputFileName).Count();
            decimal roundingNumber = Math.Ceiling(Convert.ToDecimal( totalLines/10000)) * 10000;
            decimal perthread = roundingNumber / 10;
            int perThreads = Convert.ToInt32(perthread);
            

            Task.Factory.StartNew(() => copyFileTPL(inputFileName, "output1.csv", 0* perThreads, 1* perThreads, 1)).ContinueWith((prev) => copyFileTPL(inputFileName, "output6.csv", 5* perThreads, 6* perThreads, 6));
            Task.Factory.StartNew(() => copyFileTPL(inputFileName, "output2.csv", 1 * perThreads, 2* perThreads, 2)).ContinueWith((prev) => copyFileTPL(inputFileName, "output7.csv", 6* perThreads, 7* perThreads, 7));
            Task.Factory.StartNew(() => copyFileTPL(inputFileName, "output3.csv", 2 * perThreads, 3* perThreads, 3)).ContinueWith((prev) => copyFileTPL(inputFileName, "output8.csv", 7* perThreads, 8* perThreads, 8));
            Task.Factory.StartNew(() => copyFileTPL(inputFileName, "output4.csv", 3 * perThreads, 4* perThreads, 4)).ContinueWith((prev) => copyFileTPL(inputFileName, "output9.csv", 8* perThreads, 9* perThreads, 9));
            Task.Factory.StartNew(() => copyFileTPL(inputFileName, "output5.csv", 4 * perThreads, 5* perThreads, 5)).ContinueWith((prev) => copyFileTPL(inputFileName, "output10.csv", 9* perThreads, 10* perThreads, 10));
            Console.ReadKey();

            #region commented
            //parallel invocation of the file copy task
            //you can invoke many parallel tasks this way
            //Parallel.Invoke(

            //  () =>
            //  {
            //      //start the file copy operation
            //      copyFileTPL(inputFileName, "new1.csv",1,5000,1);
            //  },
            //  () =>
            //  {
            //      //start 2nd the file copy operation
            //      copyFileTPL(inputFileName, "new2.csv", 5001, 10000, 2);
            //  },
            //  () =>
            //  {
            //      //start 2nd the file copy operation
            //      copyFileTPL(inputFileName, "new3.csv", 10001, 15000, 3);
            //  },
            //  () =>
            //  {
            //      //start 2nd the file copy operation
            //      copyFileTPL(inputFileName, "new4.csv", 15001, 20000, 4);
            //  },
            //  () =>
            //  {
            //      //start 2nd the file copy operation
            //      copyFileTPL(inputFileName, "new5.csv", 20001, 25000, 5);
            //  }

            //);
            #endregion
        }

        private static void copyFileTPL(string sourceFile, string targetFile,int startLine,int endLine,int threadNo)
        {

            Console.WriteLine();
            Console.WriteLine("File Copy Operation using the Task Parallel Library");
            Console.WriteLine();
            Console.WriteLine("File to copy: " + @sourceFile);
            //count file lines
            StreamReader sr = new StreamReader(@sourceFile);
            sr.Close();

            string line = "";
            float progress = 0;

            Console.WriteLine();
            Console.WriteLine("Copying file...");

            int count = 0;

            //file copy operations
            using (StreamReader reader = new StreamReader(@sourceFile))
            {
                StreamWriter writer = new StreamWriter(@targetFile);

                while ((line = reader.ReadLine()) != null)
                {
                     ++count;
                    if (count > startLine && count <= endLine)
                    {
                        //avoid adding blank line in the end of file
                        if (count != totalLines)
                            writer.WriteLine(line);
                        else
                            writer.Write(line);

                        progress = (count / totalLines) * 100;
                        Console.WriteLine("Thread" + threadNo + ": " + Math.Round(progress, 2).ToString() + "% (rows copied: " + count.ToString() + ")");
                    }
                    
                }

                writer.Close();
            }

            Console.WriteLine();
            Console.WriteLine("Original File: " + @sourceFile);
            Console.WriteLine("New File: " + @targetFile);

        }
        //private static void copySecondFileTPL(string sourceFile, string targetFile)
        //{

        //    Console.WriteLine();
        //    Console.WriteLine("File Copy Operation using the Task Parallel Library");
        //    Console.WriteLine();
        //    Console.WriteLine("File to copy: " + @sourceFile);

        //    //count file lines
        //    StreamReader sr = new StreamReader(@sourceFile);
        //    float totalLines = File.ReadLines(@sourceFile).Count();
        //    sr.Close();

        //    string line = "";
        //    float progress = 0;

        //    Console.WriteLine();
        //    Console.WriteLine("Copying file...");

        //    int count = 0;

        //    //file copy operations
        //    using (StreamReader reader = new StreamReader(@sourceFile))
        //    {
        //        StreamWriter writer = new StreamWriter(@targetFile);

        //        while ((line = reader.ReadLine()) != null)
        //        {
        //            ++count;
        //            if (count > 5000 && count <= 10000)
        //            {

        //                //avoid adding blank line in the end of file
        //                if (count != totalLines)
        //                    writer.WriteLine(line);
        //                else
        //                    writer.Write(line);

        //                progress = (count / totalLines) * 100;
        //                Console.WriteLine("Progress 2: " + Math.Round(progress, 2).ToString() + "% (rows copied: " + count.ToString() + ")");
        //            }

        //        }

        //        writer.Close();
        //    }

        //    Console.WriteLine();
        //    Console.WriteLine("Original File: " + @sourceFile);
        //    Console.WriteLine("New File: " + @targetFile);
        //}
        //private static void copyThirdFileTPL(string sourceFile, string targetFile)
        //{

        //    Console.WriteLine();
        //    Console.WriteLine("File Copy Operation using the Task Parallel Library");
        //    Console.WriteLine();
        //    Console.WriteLine("File to copy: " + @sourceFile);

        //    //count file lines
        //    StreamReader sr = new StreamReader(@sourceFile);
        //    float totalLines = File.ReadLines(@sourceFile).Count();
        //    sr.Close();

        //    string line = "";
        //    float progress = 0;

        //    Console.WriteLine();
        //    Console.WriteLine("Copying file...");

        //    int count = 0;

        //    //file copy operations
        //    using (StreamReader reader = new StreamReader(@sourceFile))
        //    {
        //        StreamWriter writer = new StreamWriter(@targetFile);

        //        while ((line = reader.ReadLine()) != null)
        //        {
        //            ++count;
        //            if (count >= 10000 && count <= 15000)
        //            {

        //                //avoid adding blank line in the end of file
        //                if (count != totalLines)
        //                    writer.WriteLine(line);
        //                else
        //                    writer.Write(line);

        //                progress = (count / totalLines) * 100;
        //                Console.WriteLine("Progress 3: " + Math.Round(progress, 2).ToString() + "% (rows copied: " + count.ToString() + ")");
        //            }

        //        }

        //        writer.Close();
        //    }

        //    Console.WriteLine();
        //    Console.WriteLine("Original File: " + @sourceFile);
        //    Console.WriteLine("New File: " + @targetFile);
        //}
        //private static void copyFourthFileTPL(string sourceFile, string targetFile)
        //{

        //    Console.WriteLine();
        //    Console.WriteLine("File Copy Operation using the Task Parallel Library");
        //    Console.WriteLine();
        //    Console.WriteLine("File to copy: " + @sourceFile);

        //    //count file lines
        //    StreamReader sr = new StreamReader(@sourceFile);
        //    float totalLines = File.ReadLines(@sourceFile).Count();
        //    sr.Close();

        //    string line = "";
        //    float progress = 0;

        //    Console.WriteLine();
        //    Console.WriteLine("Copying file...");

        //    int count = 0;

        //    //file copy operations
        //    using (StreamReader reader = new StreamReader(@sourceFile))
        //    {
        //        StreamWriter writer = new StreamWriter(@targetFile);

        //        while ((line = reader.ReadLine()) != null)
        //        {
        //            ++count;
        //            if (count >= 15000 && count <= 20000)
        //            {

        //                //avoid adding blank line in the end of file
        //                if (count != totalLines)
        //                    writer.WriteLine(line);
        //                else
        //                    writer.Write(line);

        //                progress = (count / totalLines) * 100;
        //                Console.WriteLine("Progress 4: " + Math.Round(progress, 2).ToString() + "% (rows copied: " + count.ToString() + ")");
        //            }

        //        }

        //        writer.Close();
        //    }

        //    Console.WriteLine();
        //    Console.WriteLine("Original File: " + @sourceFile);
        //    Console.WriteLine("New File: " + @targetFile);
        //}
        //private static void copyFifthFileTPL(string sourceFile, string targetFile)
        //{

        //    Console.WriteLine();
        //    Console.WriteLine("File Copy Operation using the Task Parallel Library");
        //    Console.WriteLine();
        //    Console.WriteLine("File to copy: " + @sourceFile);

        //    //count file lines
        //    StreamReader sr = new StreamReader(@sourceFile);
        //    float totalLines = File.ReadLines(@sourceFile).Count();
        //    sr.Close();

        //    string line = "";
        //    float progress = 0;

        //    Console.WriteLine();
        //    Console.WriteLine("Copying file...");

        //    int count = 0;

        //    //file copy operations
        //    using (StreamReader reader = new StreamReader(@sourceFile))
        //    {
        //        StreamWriter writer = new StreamWriter(@targetFile);

        //        while ((line = reader.ReadLine()) != null)
        //        {
        //            ++count;
        //            if (count >= 20000 && count <= 25000)
        //            {

        //                //avoid adding blank line in the end of file
        //                if (count != totalLines)
        //                    writer.WriteLine(line);
        //                else
        //                    writer.Write(line);

        //                progress = (count / totalLines) * 100;
        //                Console.WriteLine("Progress 5: " + Math.Round(progress, 2).ToString() + "% (rows copied: " + count.ToString() + ")");
        //            }

        //        }

        //        writer.Close();
        //    }

        //    Console.WriteLine();
        //    Console.WriteLine("Original File: " + @sourceFile);
        //    Console.WriteLine("New File: " + @targetFile);
        //}

    }
}
